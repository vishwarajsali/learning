# REST API

## Rules you should follow if you want to create a RESTful API

1. __Client__ 
    - person or a software who uses the API.
    - as a developer, can use Twitter API to read and write data, create a new tweet. 

1. __Resource__ 
    - can be any object the API can provide info about.
    - Instgram's API, resource can be a user, a photo, a hashtag.

## REST - (REpresentational State Transfer)

- A RESTful web application expose infomation about itself in the form of information about its resources. 
- it also enables the client to take action on those resources
- when a RESTful API is called, the server will transfer to the client a representation of the state of the requested resource.

- The representation of the state can be a JSON fromat, and probablu for most APIs this is indeed the case. It can be XML or HTML format.

what server does when you call the APIs depends on what information you provide to the server.

1. An identifiers for the resource. 
1. the opration you want to server to perform on the resource in the form of HTTP method, or verb (GET, POST, Put, and DELETE).

In order for API to be RESTful, it should have to be 6 constraints.

* uniform Inteface
* client 
* stateless
* layered system
* cacheble
* code-on-demand

- Uniform interface
  - request to server has to include a resource identifier.
  
  - response the server returns incluude enough infromation so that client can modify
  
  - each request contains all the information the server needs to perform request, and each response the server return contains all the information the client needs in order to understand.

- Client - Server Separation
  - both act independently 
  
  - only interaction between them is form of request, initiated by the client only and responses, which the server send to client.
  
  - server waiting for requests from the client. 
  - 
- Stateless
  - the server does not remember anything about the user who uses the API. 
  
  - each request contains all the information the server needs to perform the request and return a response.

- Layered system
  - between the client who request a representation of a resources's state and the server who sends the response back, ther might be number of servers in the middle. 
  
  - They provide a security, caching, load-balancing or other functionality.
  
- Cacheable 
  
  - data server sends contains information about whether or not the data is cacheable. 

  - if cacheable might have version number.

  - because of the version number it is easy to keep track which data is stored 

  - the client can void requesting the sa,e data again

-  Code-on-demand

   - is optional

   - response from the server will contain some code. in the form of script 


