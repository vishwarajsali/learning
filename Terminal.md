# Terminal

- `cd` - Change Directory

- `cd..` - get back to previous directory
  
- `ls` - list all the files in the directory

- `!!` - execute last command
  
- `clear` - clear terminal

- `pwd` - path of the directory

- `ls -a` - show all the files with hidden file

- `ls -l` - directory with details format

- `ls -la` - show details of the hidden files

- `ls <directoryName>` - show all the files in that directory

- `ls -la <directory>` - show all the files with details within directory

- `mkdir <directory>` - create a new directory

- `vim <fileName>` - create a file and show to edit
  - `i` - to insert
  - `esc` - to write command in terminal
  - `wq` - write and quite file
  - `!q` - descard changes in file

- `cat <fileName>` - print file in the terminal.

- `nano <filename>` - create and open in the new editor

- `open <fileName>` - open file in the text editor.

- `mv <fileName> <directory>` - move file file from one directory to another.

- `mv <OldFileName> <NewFileName>` - chaneg the name of the file

- `mv <directory>/*.txt` - move all file with the text format

- `rm <fileName>` - remove file

- `rm -R <directory>` - remove all  file in and directory.

- `cp <OldFile> <NewFileName>` - copy file

- `~` - Home directory

- `cp -R <directory> <path>` - copy directory with all the files

- `sudo shutdown -r now` - restart computer now

- `sudo shutdown -h <+30/+60>` - shutdown computer with the min

