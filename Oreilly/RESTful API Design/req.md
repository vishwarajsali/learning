## Set up
- Node
- mongoDB Community server
- mongodb Compass
- postman


### API 

- Application programming Interface
- two software to communicate
- abstract the implementation of one piece of software to other
  

### RESTful API
- set of guidelines to build a web service
- HTTP methods and status code
-  stateless (no session)
-  data return HTML, JSON, XML, and plain text
-  CURD oprations on resources

#### HTTP Methods
  - GET - get resource
  - POST - create resource
  - PUT/PATCH - update Resource
  - DELETE - delate resource

#### Status Code
   - 1** - information response
   - 2** - Successful response 
   - 3** - Redirectional Messages
   - 4** - Client error response
   - 5** - Server error response
  
  - 200 Sucess
  - 404 Not found
  - 400 Bad request
  - 401 Unathorized
  - 500 Internal server error
  - 