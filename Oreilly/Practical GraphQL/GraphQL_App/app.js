const express = require('express');
const app = express();
const { ApolloServer, gql } = require('apollo-server-express');
let users = require('./data').users;
const me = users[0];
let cars = require('./data').cars;


const typeDefs = gql`
    type Query{
        users:[User]
        cars: [Car]
        user(id: Int!):User
        car(id: Int!):Car
        me: User
    }

    type User{
        id: ID!
        name: String!
        car: [Car]
    }

    type Car{
        id: ID!
        make: String!
        model :String!
        color: String!
        owner: User!

    }

    type Mutation{
        makeUser(id: Int!, name: String!): User!
        removeUser(id: Int!): Boolean
        makeCar(id: Int!, make: String!): Car!
    }
`;
const resolvers = {
    Query:{
        users:() => users,
        me: () => me,
        user: (parents, {id}) =>{
            const user = users.filter(user => user.id === id);
            return user[0];
        } ,

        cars:() => cars,
        car:(parents, {id}) => {
            const car = cars.filter(car => car.id === id);
            return car[0];
        }
        
    },
    Mutation:{
        makeUser: (parent, {id, name}) => {
            const user ={
                id,
                name
            };
            users.push(user);
            return user;
        },
        removeUser:(parent, {id})=>{
            let found = false;
            users = users.filter(user => {
                if(user.id === id){
                    found = true;
                }
                else return user;
            });

            if(found){
                return true;
            }else return false;
        }
    },
    Car:{
        owner: parent => users[parent.ownedBy-1]
    },
    User: {
        car: parent => {
           return parent.cars.map(carId => cars[carId-1])
        }
    }
};

const server = new ApolloServer({
    typeDefs,
    resolvers
});

server.applyMiddleware({app});

app.listen(3000, () => console.info('Apollo GraphQl server is running on port 3000'));

