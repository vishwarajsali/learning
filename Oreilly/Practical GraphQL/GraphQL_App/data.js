let users = [{
    id: 1,
    name: 'Vishwaraj',
    cars:[1,4]
}, {
    id: 2,
    name: 'Jean',
    cars: [2]
}, {
    id: 3,
    name: 'Rose',
    cars:[3]
}];


let cars = [{
    id: 1,
    make: 'Honda',
    model: 'Civic',
    color: 'Black',
    ownedBy: 1
},{
    id: 2,
    make: 'Fiat',
    model: '500',
    color: 'Yellow',
    ownedBy: 2
},{
    id: 3,
    make: 'Ford',
    model: 'Focus',
    color: 'Red',
    ownedBy: 3
},{
    id: 4,
    make: 'Mercedes',
    model: 'C250',
    color: 'Black',
    ownedBy: 1
}
];



module.exports = {
    users,
    cars
};