# Practical GraphQL

## What is GraphQL

    - Query Language for API
    - REST vs GraphQL
      - Single vs Multiple request
      - get the data you need exactly
    - uses a type system
    - Doesn't rely on any db or storage

## Schema

- also referred to as type definitions(type defs)
- specifies that available data for reading/ writing
- Has a mandotory Query type
  - defines what to query for
- works using fields and nested fields
- uses SDL - schema definition language 
  - schema doesn't care where that data is coming from.

## schemas and types

- field values use scalars - String, int, boolean etc.
- type specifies that type for a given field
- custom types can be added (like User)
- Exclamation mark : non-nullable field
- ID : internal identifier for advanced usage(eg. caching)
  
```graphql
type User{
    id: ID
    name: String!
    email: String
}

type Query{
    users: [User]
    user(id:ID!): User
}
```

## Resolver 

- function that defines where the data is coming from 
- Resolver Map - a javascrup object of resolvers
- each top level Query type needs a resolver
- Works with 4 parameters : object, arguments, context, info
    - Object : contains the result on the parent
    - Args: get employee with id 12
    - context : an object shared by all resolvers - useful for authentication!
    - info: rarely used, contains execution information.

## Query

```graphql
{
    users{
        name,
        email
    }
}

{
    "data" :{
        "users:[{
            "name" : "John",
            "email" : "John@gmail.com"
        },{
            "name" :"Sue",
            "email": "sue@gmail.com"
        }]
    }
}
```

Creating the application shell
we gonna use apollo-server for this project

```shell
npm i express graphql apollo-server apollo-server-express
```

with this command we are going to use the this dependencies 
 
use `nodemon app.js` to run on server

to install nodemon globally
`npm i -g nodemon`

use the link http://localhost:3000/graphql 