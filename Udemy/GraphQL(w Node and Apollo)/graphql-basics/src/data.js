
export const postsData = [{
    id: '1',
    title: 'Intro to Git',
    body: 'Welcome to git ',
    published: true,
    author: '1'
},{
    id: '2',
    title: 'Intro to GraphQL',
    body: 'Welcome to GraphQL',
    published: true,
    author: '1'
},{
    id: '3',
    title: 'biginner to Python',
    body: 'Python Tutorial',
    published: true,
    author: '2'    
}]

export const usersData = [{
    id: '1',
    name: 'Vish',
    email: 'vish@gmail.com',
    age: 28
},{
    id: '2',
    name: 'Andrew',
    email: 'andrew@yahoo.com'
},{
    id: '3',
    name: 'Kris',
    email: 'kris@example.com',
    age: 28
},{
    id: '4',
    name: 'Sarah',
    email: 'sarah@gmail.com',
    age: 53
}]

export const commentsData= [{
    id: '1',
    text: "Nice Tutorial",
    post: '1',
    author: '1'
},{
    id:'2',
    text: 'Great Article',
    post: '1',
    author:'1'
},{
    id: '3',
    text: 'Nice',
    post: '2',
    author:'2'
},{
    id: '4',
    text: 'Create More Tutorial',
    post: '3',
    author:'3'
}
]

