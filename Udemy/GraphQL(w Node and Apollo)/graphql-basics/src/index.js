import {
    GraphQLServer
} from 'graphql-yoga'

import {usersData, postsData, commentsData} from './data'

const typeDefs = `
    type Query {
        users(query: String): [User!]!
        posts(query: String): [Post!]!
        comments(query: String): [Comment!]!
        me: User!
        post: Post
       
    }

    type User{
        id: ID!
        name: String!
        email: String!
        age: Int
        posts: [Post!]!
        comments: [Comment!]!
    }
     
    type Post{
        id: ID!
        title: String!
        body: String!
        published: Boolean!
        author: User!
        comments: [Comment!]!
    }

    type Comment{
        id: ID!
        text: String!
        post: Post!
        author: User!
    }
`

// Resolvers
const resolvers = {
    Query: {
        users(parent, args, ctx, info){
            if(!args.query){
             return usersData;
            }
             return usersData.filter((user) => {
                return user.name.toLowerCase().includes(args.query.toLowerCase())
            })
        },
        posts(parent, args, ctx, info){
            if(!args.query) return postsData;
            return postsData.filter((post) => {
                return post.title.toLowerCase().includes(args.query.toLowerCase()) || post.body.toLowerCase().includes(args.query.toLowerCase())
            })
        },
        me() {
            return usersData[0];
        },
        post() {
            return postsData[0];
        },
        comments(parent, args, ctx, info){
            if(!args.query) return commentsData
            return commentsData.filter((comment)=>{
                return comment.id.toLowerCase().includes(args.query.toLowerCase())
            })
        }

    },
    Post: {
        author(parent, args, ctx, info){
            return usersData.find((user) => {
                return user.id == parent.author 
            })
        },
        comments(parent, args, ctx, info){
            return commentsData.filter((comment) => {
                return comment.post == parent.id
            })
        }
    },

    User:{
        posts(parent, args, ctx, info){
            return postsData.filter((post)=> {
                return post.author == parent.id
            })
        },
        comments(parent,args, ctx, info){
            return commentsData.filter((comment)=> {
                return comment.author == parent.id
            })
        }
    },
    Comment:{
        author(parent, args, ctx, info){
            return usersData.find((user) => {
                return user.id == parent.author
            })
        },
        post(parent, args, ctx, info){
            return postsData.find((post)=>{
                return post.id == parent.post
            })
        }
    }
}

const server = new GraphQLServer({
    typeDefs,
    resolvers
})

server.start(() => {
    console.log('The server is running!')
})