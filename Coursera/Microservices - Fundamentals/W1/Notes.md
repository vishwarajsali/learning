## Microservice 
An engineering approach focused on decomposing applications into single-function modules with well-defined interfaces which are independently deployed and operated by small teams who own the entire lidecycle od the service.  
Microservices acclerate delivery by minimizing communication and coordination between people while reducing the scope and risk of change.
