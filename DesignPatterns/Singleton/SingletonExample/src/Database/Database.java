package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class Database {
	Connection conn;
	Statement statement;
	
	private static Database instance;
	
	private Database() {
		String url= "jdbc:mysql://localhost:3306/";
        String dbName = "Java";
        String driver = "com.mysql.cj.jdbc.Driver";
        String userName = "root";
        String password = "root";
        try {
            Class.forName(driver);
            this.conn = (Connection)DriverManager.getConnection(url+dbName,userName,password);
            System.out.println("Connected----->>");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}

	public static Database getInstance() {
		if(instance == null) {
			instance = new Database();
		}
		return instance;
	}
	
	public ResultSet query(String query) throws SQLException{
        statement = conn.createStatement();
        return statement.executeQuery(query);

    }

    public int insert(String insertQuery) throws SQLException {
        statement = conn.createStatement();
        return statement.executeUpdate(insertQuery);    
            
    }
    
   


}
