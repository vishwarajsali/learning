package DP;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * FibonacciSeries
 */
public class FibonacciSeries {

	static HashMap<Integer, Integer> map = new HashMap<>();
	
    public static void main(String[] args)  {

		System.out.println(fibDP(10));
		
	}

    public static int fibRec(int n){
        if( n <=1) return n;
        return fibRec(n-2) + fibRec(n-1);
	}
	
	public static int fibDP(int n){
		if(map.containsKey(n)) return map.get(n);
		if(n <= 1) return n;
		map.put(n, (fibDP(n-1)+fibDP(n-2)));
		return map.get(n);
	}
}