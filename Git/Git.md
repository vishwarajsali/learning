# Git

- git init - Create a new Repository
- git status - shows the all the files which are not commited to repository and shows the staging 
- git add "\<File Name\>" - add the specific file to commit
- git add --all / git add * / git add . - add all files to commit in local repository
- git commit -m "\<Commit Message\>" - it add the files to the commit with the commit message
- git push - push all the commit to the remote repository
- git pull origin master - pull all the changes or all the files from remote repository to local repository
  