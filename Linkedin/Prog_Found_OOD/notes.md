## Programming Foundations: Object-Oriented Design 
[Course](https://www.linkedin.com/learning/programming-foundations-object-oriented-design-3/learn-object-oriented-design-principles)



example of roundCookie class

```mermaid
classDiagram
class RoundCookie{
    weight
    color
    icing
    decorate()
    consume()
}

```  

**A**bstraction  
**P**olymorphism  
**I**nheritance  
**E**encapsulation


### Inheritance
* base a new object or class on an existing one 
* inherit the existing attributes and methods
* great from of code reuse


```mermaid
classDiagram
Person <|-- Customer
Person <|-- Employee
Person <|-- Courier

class Person{
    name
    phone
    address
    email
    updateContact()
}

class Customer{
    customerID
    purchase()
}


class Employee{
    employeeID
    promote()
    retire()
}

class Courier{
    deliverPackage()
}
```

### Polymorphism

* Dynamic Polymorphism
  * user same interface for methods on different types of objects 
  * 




```mermaid
classDiagram
 Shape "1" --> "*" Color
 
class Shape{
    <<interface>>
    noOfVertices
    draw()
}
class Color{
    <<enumeration>>
    RED
    BLUE
    GREEN
    WHITE
    BLACK
}
```    

 