# Learning REST APIs

[Link](https://www.linkedin.com/learning/learning-rest-apis/)

## Representational State Transfer (REST)

group of software architecture design contrains that bring about efficent,reliable and scalable system.

## Application Programming Interface(APIs)

A set if features and rules that exist inside a software program enabling interaction between that software and other items such as other software or hardware.

## Universal Resource Identifier (URI)

A compact sequence of characters that identifies an abstract or physical resource that provides a simple and extensible means for identifing a resource.

## Universal Recource Locator (URL)

Subset of URI that identifies a resource and ecplain how to access that resource by providing an explicit method link https:// or ftp://

## The Six Contraints of REST

1. Client - server architecture
   1. The client manages user interface concerns while that server manages data storage concerns. 

1. Statelessness
   1. No client context or information aka state can be stored on that server between request.

1. Cacheability
   1. All REST responses must be clearly marked as cacheable or not cacheable.

1. Layered System
   1. The client cannot know, and should not care, whether its connected directly to the server or to an intermediary like a CDN or mirror
   
1. code on demand
   1. Servers are allowed to transfer executable code like javascript and compiled components to clients.

2.  Uniform interface
    1.  Resource identification in requests - The URI request must specify what resource it is looking for and what format the response should be 
    2.  Resource manupulation through representation - once a client has a representation of a resource, it can modify or delete the resource.
    3.  Self-descriptive messages - each representatino must describe its own data format.
    4.  Hypermedia as the engin of application- once a client has access to a REST service, it should be able to discover all available resources and methods through the hyperlinks provided.
   

[Reqres API](https://reqres.in)

## Methods (Verbs)

### Get 
get the data 
specified resources if available
- success 
  - 200 Ok
- failure
  - 404 Not Found 
  
### Send data 
- post 
- put 
- patch

### Post
Create a new resource and add it to a collection

- success 
  - 201 Created - create and gives the id in header 
- failure
  - 401 Unauthorized
  - 409 conflict - already exist
  - 404 Not found 

### put
Update an existing singleton resource based on ID

- success 
  - 200 Ok
- failure
  - 401 Unauthorized
  - 404 Not found 
  - 405 Method Not Allowed

### Patch
Modify an existing singleton resource based on ID

- success 
  - 200 ok
- failure
  - 401 Unauthorized
  - 404 Not found 
  - 405 Method Not Allowed


### Delete 
Delete singleton resource based on ID

- success 
  - 200 ok
- failure
  - 401 Unauthorized
  - 404 Not found 

### Options 
get the options available from this resouce 

- success 
  - 200 ok
  
### Head 
get the response headers from the resouces 

- success 
  - 200 ok
- failure 
  - 404 Not Found


## HTTP status Messages

- 1XX   Infromation
- 2XX   Success
  - 200     OK
  - 201     Created
  - 204     No Content
- 3XX   Redirection
  - 301     Moved permanently
  - 302/303 Found at this other URL
  - 307     temporary redirect
  - 308     Resume incomplete
- 4XX   Client Error
  - 400     Bad request
  - 401     Unauthorized
  - 403     Forbidden
  - 404     Not Found
  - 405     Method not allowed
- 5XX   Server Error
  - 500     Interanl server error
  - 502     Bad gateway
  - 503     service unavailable

